import React, { FC, useEffect } from 'react';
import { useLocation, Navigate } from 'react-router-dom';
import { useAppDispatch } from '../shared/hooks';
import { refreshAccessToken } from '../store/slices/authSlice';

const RequireAuth: FC<{ children: React.ReactNode }> = ({ children }) => {
    const location = useLocation();
    const storedAccessToken = localStorage.getItem('accessToken');
    const storedRefreshToken = localStorage.getItem('refreshToken');
    const dispatch = useAppDispatch();

    useEffect(() => {
        if (!storedAccessToken && storedRefreshToken) {
            dispatch(refreshAccessToken(storedRefreshToken));
        }
    }, [dispatch, storedAccessToken, storedRefreshToken]);

    if (!storedAccessToken && !storedRefreshToken) {
        return <Navigate to="/login" state={{ from: location }} />;
    }

    return <>{children}</>;
};

export default RequireAuth;
