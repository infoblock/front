import instance from './instance';

const getEditors = async () => {
    const response = await instance.get(`users`);
    return response.data;
};

const addEditors = async (email: string) => {
    const response = await instance.post(`users/invite?email=${email}`);
    return response;
};

const banEditors = async (id: string) => {
    const response = await instance.put(`users/${id}/ban`);
    return response.data;
};

const unbanEditors = async (id: string) => {
    const response = await instance.put(`users/${id}/unban`);
    return response.data;
};

const EditorsService = {
    getEditors,
    addEditors,
    banEditors,
    unbanEditors,
};

export default EditorsService;
