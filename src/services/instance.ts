import axios from 'axios';
import AuthService from './auth.service';
import NewsService from './news.service';
import { NewsType } from '../constants/enums';

const instance = axios.create({
    baseURL: 'http://51.250.93.99/api/',
    headers: { Authorization: `Bearer ${localStorage.getItem('accessToken')}` },
});

instance.interceptors.response.use(
    (response) => {
        return response;
    },
    async (error) => {
        // const originalRequest = error.config;
        // if (error.response.status === 401) {
        //     try {
        //         const currentRefreshToken = localStorage.getItem('refreshToken') || '';
        //         const response = await AuthService.refreshToken(currentRefreshToken);
        //
        //         const newAccessToken = response.accessToken;
        //         localStorage.setItem('accessToken', newAccessToken);
        //         instance.defaults.headers.common['Authorization'] = `Bearer ${newAccessToken}`;
        //
        //         return instance(originalRequest);
        //     } catch (error) {
        //         return Promise.reject(error);
        //     }
        // }
        console.warn(error);
        return Promise.reject(error);
    }
);

export default instance;
