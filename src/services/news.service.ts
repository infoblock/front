import { AxiosRequestConfig } from 'axios';
import { FormDataResponse, FormData, StateSearch } from '../constants/types';
import instance from './instance';

const getNews = async (params: StateSearch) => {
    const config: AxiosRequestConfig = {
        params: {
            NewsType: params.newsType,
            query: params.query,
            author: params.author,
            tags: params.tags,
            audienceTypes: params.audienceTypes,
            locations: params.location,
            page: params.page,
            count: params.count,
            sortField: params.sortField,
            sortDirection: params.sortDirection,
        },
    };
    const response = await instance.get(`news`, config);
    return response;
};

const getNewsItem = async (id: string | undefined) => {
    const response = await instance.get(`news/${id}`);
    return response.data;
};

const deleteNewsItem = async (id: string | undefined) => {
    const response = await instance.delete(`news/${id}`);
    return id;
};

const postNews = async (postNewsData: FormData): Promise<FormDataResponse> => {
    const response = await instance.post('news', postNewsData);
    return response.data;
};

const patchNews = async (postNewsData: FormData): Promise<FormDataResponse> => {
    const response = await instance.patch(`news/${postNewsData.id}`, postNewsData);
    return response.data;
};

const NewsService = {
    getNews,
    getNewsItem,
    postNews,
    patchNews,
    deleteNewsItem,
};

export default NewsService;
