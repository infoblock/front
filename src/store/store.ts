import { configureStore } from '@reduxjs/toolkit';
import authReducer from './slices/authSlice';
import newsReducer from './slices/newsSlice';
import searchReducer from './slices/searchSlice';
import editorsReducer from './slices/editorsSlice';

const store = configureStore({
    reducer: {
        auth: authReducer,
        news: newsReducer,
        search: searchReducer,
        editors: editorsReducer,
    },
});
export default store;

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
