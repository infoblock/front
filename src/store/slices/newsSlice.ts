import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import NewsService from '../../services/news.service';
import { FormData, StateNews, StateSearch } from '../../constants/types';

const NEWS_STATE: StateNews = {
    foundEntities: [],
    totalCount: 1,
    item: null,
};
export const getNewsThunk = createAsyncThunk('news/getNews', async function (_, { rejectWithValue, getState }) {
    try {
        const { search } = getState() as { search: StateSearch };
        const response = await NewsService.getNews(search);
        return response.data;
    } catch (error) {
        return rejectWithValue(error);
    }
});

export const getNewsItemThunk = createAsyncThunk(
    'news/getNewsItem',
    async function (id: string | undefined, { rejectWithValue }) {
        try {
            return await NewsService.getNewsItem(id);
        } catch {
            return rejectWithValue('Server Error!');
        }
    }
);

export const deleteNewsItemThunk = createAsyncThunk(
    'news/deleteNewsItem',
    async function (id: string | undefined, { rejectWithValue }) {
        try {
            return await NewsService.deleteNewsItem(id);
        } catch {
            return rejectWithValue('Server Error!');
        }
    }
);

export const postNewsThunk = createAsyncThunk('news/postNews', async (newsSliceData: FormData, thunkAPI) => {
    try {
        return await NewsService.postNews(newsSliceData);
    } catch (error: Error | Response | any) {
        if (error instanceof Response) {
            return thunkAPI.rejectWithValue(await error.json());
        }
        return thunkAPI.rejectWithValue({ message: error.message });
    }
});

export const patchNewsThunk = createAsyncThunk('news/patchNews', async (newsSliceData: FormData, thunkAPI) => {
    try {
        return await NewsService.patchNews(newsSliceData);
    } catch (error: Error | Response | any) {
        if (error instanceof Response) {
            return thunkAPI.rejectWithValue(await error.json());
        }
        return thunkAPI.rejectWithValue({ message: error.message });
    }
});

const newsSlice = createSlice({
    name: 'news',
    initialState: NEWS_STATE,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(getNewsThunk.fulfilled, (state, action) => {
                state.foundEntities = action.payload.foundEntities;
                state.totalCount = action.payload.totalCount;
            })
            .addCase(getNewsThunk.pending, (state, action) => {
                state.foundEntities = [];
                state.totalCount = 0;
            })
            .addCase(getNewsThunk.rejected, (state, action) => {})
            .addCase(getNewsItemThunk.fulfilled, (state, action) => {
                state.item = action.payload;
            })
            .addCase(getNewsItemThunk.pending, (state, action) => {
                state.item = null;
            })
            .addCase(postNewsThunk.fulfilled, (state, action) => {})
            .addCase(postNewsThunk.rejected, (state, action) => {
                console.error('postNewsThunk.rejected');
                console.error(action.payload);
            })
            .addCase(patchNewsThunk.fulfilled, (state, action) => {})
            .addCase(deleteNewsItemThunk.fulfilled, (state, action) => {
                state.foundEntities = state.foundEntities.filter((item) => item.id !== action.payload);
            });
    },
});

export const {} = newsSlice.actions;
export default newsSlice.reducer;
