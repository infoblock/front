import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { StateSearch } from '../../constants/types';
import { NewsType, SortDirection, SortField } from '../../constants/enums';

const SEARCH_STATE: StateSearch = {
    query: null,
    author: null,
    tags: null,
    audienceTypes: null,
    location: null,
    newsType: null,
    page: null,
    count: 50,
    sortField: null,
    sortDirection: null,
};

const searchSlice = createSlice({
    name: 'search',
    initialState: SEARCH_STATE,
    reducers: {
        setQuery: (state, action: PayloadAction<string | null>) => {
            state.query = action.payload;
        },
        setAuthor: (state, action: PayloadAction<string | null>) => {
            state.author = action.payload;
        },
        setTags: (state, action: PayloadAction<string[] | null>) => {
            state.tags = action.payload;
        },
        setAudienceTypes: (state, action: PayloadAction<string[] | null>) => {
            state.audienceTypes = action.payload;
        },
        setLocation: (state, action: PayloadAction<string[] | null>) => {
            state.location = action.payload;
        },
        setNewsType: (state, action: PayloadAction<NewsType | null>) => {
            state.newsType = action.payload;
        },
        setPage: (state, action: PayloadAction<number | null>) => {
            state.page = action.payload;
        },
        setSortField: (state, action: PayloadAction<SortField | null>) => {
            state.sortField = action.payload;
        },
        setSortDirection: (state, action: PayloadAction<SortDirection | null>) => {
            state.sortDirection = action.payload;
        },
    },
});

export const {
    setQuery,
    setAuthor,
    setTags,
    setAudienceTypes,
    setLocation,
    setNewsType,
    setPage,
    setSortField,
    setSortDirection,
} = searchSlice.actions;
export default searchSlice.reducer;
