import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import EditorsService from '../../services/editors.service';
import { StateEditors } from '../../constants/types';

const EDITORS_STATE: StateEditors = {
    foundEntities: [],
    totalCount: 0,
    errorMessage: null,
    isSuccess: false,
};
export const getEditorsThunk = createAsyncThunk('editors/getEditors', async function (_, { rejectWithValue }) {
    try {
        return await EditorsService.getEditors();
    } catch {
        return rejectWithValue('Server Error!');
    }
});
export const addEditorThunk = createAsyncThunk(
    'editors/addEditor',
    async function (email: string, { rejectWithValue }) {
        try {
            const responce = await EditorsService.addEditors(email);
            return responce.data;
        } catch (error: any) {
            const message = error.response.data.Message;
            return rejectWithValue(message);
        }
    }
);
export const banEditorThunk = createAsyncThunk('editors/banEditor', async function (id: string, { rejectWithValue }) {
    try {
        return await EditorsService.banEditors(id);
    } catch {
        return rejectWithValue('Server Error!');
    }
});
export const unbanEditorThunk = createAsyncThunk(
    'editors/unbanEditor',
    async function (id: string, { rejectWithValue }) {
        try {
            return await EditorsService.unbanEditors(id);
        } catch {
            return rejectWithValue('Server Error!');
        }
    }
);

const editorsSlice = createSlice({
    name: 'editors',
    initialState: EDITORS_STATE,
    reducers: {
        setIsSuccess: (state, action: PayloadAction<boolean>) => {
            state.isSuccess = action.payload;
        },
    },
    extraReducers: (builder) => {
        builder
            .addCase(getEditorsThunk.fulfilled, (state, action) => {
                state.foundEntities = action.payload.foundEntities;
                state.totalCount = action.payload.totalCount;
            })
            .addCase(addEditorThunk.fulfilled, (state, action) => {
                state.isSuccess = true;
            })
            .addCase(addEditorThunk.rejected, (state, action) => {
                state.isSuccess = false;
                state.errorMessage = action.payload?.toString() || null;
            })
            .addCase(unbanEditorThunk.fulfilled, (state, action) => {
                state.foundEntities = state.foundEntities.map((e) =>
                    e.id === action.meta.arg ? { ...e, enabled: true } : e
                );
            })
            .addCase(banEditorThunk.fulfilled, (state, action) => {
                state.foundEntities = state.foundEntities.map((e) =>
                    e.id === action.meta.arg ? { ...e, enabled: false } : e
                );
            });
    },
});

export const { setIsSuccess } = editorsSlice.actions;
export default editorsSlice.reducer;
