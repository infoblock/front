import React from 'react';
import { Provider } from 'react-redux';
import { Navigate, Route, Routes } from 'react-router-dom';
import s from './App.module.css';
import store from './store/store';
import Login from './pages/auth/login';
import RequireAuth from './hoc/requireAuth';
import Editors from './pages/editors/editors';
import Draft from './pages/publications/draft';
import Layout from './components/layout/layout';
import Register from './pages/auth/registration';
import Deffered from './pages/publications/deffered';
import Statistics from './pages/statistics/statistics';
import Published from './pages/publications/published';
import Create from './pages/publications/create/create';
import ViewNews from './components/screens/viewNews/viewNews';
import EditNews from './components/screens/editNews/editNews';
import LayoutNews from './components/screens/news/layout/LayoutNews';

function App() {
    return (
        <Provider store={store}>
            <div className={s.App}>
                <Routes>
                    <Route path="login" element={<Login />} />
                    <Route path="registration" element={<Register />} />

                    <Route path="" element={<RequireAuth><Layout /></RequireAuth>}>
                        <Route index element={<RequireAuth><Navigate to="publications/published" /></RequireAuth>}/>

                        <Route path="publications" element={<RequireAuth><LayoutNews /></RequireAuth>}>
                            <Route index element={<Navigate to="published" />} />
                            <Route path="published" element={<RequireAuth><Published /></RequireAuth>}/>
                            <Route path="deferred" element={<RequireAuth><Deffered /></RequireAuth>}/>
                            <Route path="draft" element={<RequireAuth><Draft /></RequireAuth>}/>
                        </Route>

                        <Route path="statistics" element={<RequireAuth><Statistics /></RequireAuth>}/>
                        <Route path="editors" element={<RequireAuth><Editors /></RequireAuth>}/>
                        <Route path="*" element={<RequireAuth><>Страница отсутствует</></RequireAuth>}/>
                    </Route>
                    <Route path="publications/:id" element={<RequireAuth><ViewNews /></RequireAuth>}/>
                    <Route path="edit/:id" element={<RequireAuth><EditNews /></RequireAuth>}/>
                    <Route path="create" element={<RequireAuth><Create /></RequireAuth>}/>
                </Routes>
            </div>
        </Provider>
    );
}

export default App;
