import React, { FC, useState } from 'react';
import { BsArrowLeft } from 'react-icons/bs';
import CreatableSelect from 'react-select/creatable';
import { Link, useNavigate } from 'react-router-dom';
import { useForm, SubmitHandler } from 'react-hook-form';
import { ActionMeta, GroupBase, MultiValue, StylesConfig } from 'react-select';
import { useAppDispatch } from '../../../shared/hooks';
import { parseCheckbox } from '../../../helpers/parseCheckbox';
import { postNewsThunk } from '../../../store/slices/newsSlice';
import { Content, FormData, Option } from '../../../constants/types';
import { parseData, parseDataToUTC } from '../../../helpers/parseDate';
import DeletePublicationPopup from '../../deletePopup/delete';
import Preview from './preview';
import EditorNews from './editorNews';
import s from './createNews.module.css';

const options: Option[] = [
    { value: 'nalogs', label: 'налоги' },
    { value: 'sotrudniky', label: 'сотрудники' },
    { value: 'buxuchet', label: 'бухучет' },
    { value: 'prochayaOtchetnost', label: 'прочая отчетность' },
    { value: 'biznes', label: 'бизнес' },
    { value: 'instrumentyBuhgaltera', label: 'инструменты бухгалтера' },
];

const CreateNews: FC = () => {
    const [content, setContent] = useState<Content>({
        content: '<h2>Начните вашу публикацию здесь...</h2>',
    });
    const [preview, setPreview] = useState(false);

    const navigate = useNavigate();
    const dispatch = useAppDispatch();

    const deletePublication = () => {
        navigate('/');
    };
    const saveToDraft: SubmitHandler<FormData> = (data) => {
        data.tags = tagLabels;
        const [audienceValues, locationValues] = parseCheckbox(data);
        data.audienceTypes = audienceValues;
        data.locations = locationValues;

        data.body = content.content;
        data.publish = false;
        data.publicationDate = parseDataToUTC(data.publicationDate);
        dispatch(postNewsThunk(data));
        navigate('/');
    };

    const handleChange = (newValue: MultiValue<Option>, actionMeta: ActionMeta<Option>) => {
        const labels = newValue.map((v: Option) => v.label);
        setTagLabels(labels);
    };

    const onSubmit: SubmitHandler<FormData> = (data) => {
        data.tags = tagLabels;
        const [audienceValues, locationValues] = parseCheckbox(data);
        data.audienceTypes = audienceValues;
        data.locations = locationValues;

        data.body = content.content;
        data.publish = true;
        data.publicationDate = parseDataToUTC(data.publicationDate);
        dispatch(postNewsThunk(data));
        navigate('/');
    };

    const [tagLabels, setTagLabels] = useState<string[]>([]);
    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm<FormData>();

    const currentDate = new Date();
    const minDate = currentDate.toString();

    const [isOpenDelete, setIsOpenDelete] = useState(false);

    return (
        <div>
            <div className={s.headButtons}>
                <div className={s.btns}>
                    <Link to={'/'} className={s.btnDefault}>
                        <BsArrowLeft style={{ transform: 'translateY(2px)' }} /> Назад
                    </Link>
                    <button
                        className={s.btnDefault}
                        onClick={() => {
                            setPreview(!preview);
                        }}
                    >
                        {preview ? 'Редактировать' : 'Предпросмотр'}
                    </button>
                </div>

                <div className={s.btns}>
                    <button onClick={() => setIsOpenDelete(true)} className={s.btnDelete}>
                        Удалить
                    </button>
                    <button onClick={handleSubmit(saveToDraft)} className={s.btnDefault}>
                        Сохранить в черновик
                    </button>
                    <button onClick={handleSubmit(onSubmit)} className={s.btnCreate}>
                        Опубликовать
                    </button>
                </div>
            </div>
            <DeletePublicationPopup isOpenDelete={isOpenDelete} setIsOpenDelete={setIsOpenDelete} />

            <form className={s.form}>
                {/* ЗАГОЛОВОК */}
                {errors.title && <span className={s.errorMsg}>Введите заголовок публикации</span>}
                <div className={s.formItem}>
                    <label htmlFor="title" className={s.label}>
                        Заголовок*
                    </label>
                    <input
                        {...register('title', { required: true })}
                        type="text"
                        id="title"
                        className={s.input}
                        placeholder="Введите заголовок"
                    />
                </div>

                {/* АВТОР */}
                {errors.author && <span className={s.errorMsg}>Введите автора публикации</span>}
                <div className={s.formItem}>
                    <label htmlFor="author" className={s.label}>
                        Автор статьи*
                    </label>
                    <input
                        {...register('author')}
                        type="text"
                        id="author"
                        className={s.input}
                        placeholder="Введите имя автора"
                    />
                </div>

                {/* АВАТАР */}
                <div className={s.formItem}>
                    <label htmlFor="avatar" className={s.label}>
                        Ссылка на фото
                    </label>
                    <input
                        {...register('avatar')}
                        type="url"
                        id="avatar"
                        className={s.input}
                        placeholder="Введите ссылку"
                    />
                </div>

                {/* ТЕГ */}
                <div className={s.formItem}>
                    <label className={s.label}>Тег</label>
                    <CreatableSelect
                        isMulti
                        options={options}
                        placeholder={'Выберите тег'}
                        styles={customStyles}
                        theme={customTheme}
                        {...register('tags')}
                        onChange={handleChange}
                    />
                </div>

                {/* ФИЧАФЛАГ */}
                <div className={s.formItem}>
                    <label htmlFor="fichaflag" className={s.label}>
                        Фичафлаг
                    </label>
                    <input
                        type="text"
                        id="fichaflag"
                        {...register('featureFlag')}
                        className={s.input}
                        placeholder="Введите название фичафлага"
                    />
                </div>

                {/* АУДИТОРИЯ */}
                <div className={s.formItem}>
                    <label htmlFor="users" className={s.label}>
                        Аудитория*
                    </label>
                    <div className={s.inputCheckbox}>
                        <div>
                            <input
                                className={s.checkbox}
                                type="checkbox"
                                id="all"
                                {...register('all')}
                                defaultChecked={true}
                            />
                            <label htmlFor="all">Все</label>
                        </div>
                        <div>
                            <input className={s.checkbox} type="checkbox" id="abonent" {...register('abonent')} />
                            <label htmlFor="abonent">Абонент</label>
                        </div>
                        <div>
                            <input className={s.checkbox} type="checkbox" id="portUser" {...register('portUser')} />
                            <label htmlFor="portUser">Портальный пользователь</label>
                        </div>
                        <div>
                            <input className={s.checkbox} type="checkbox" id="groupOrg" {...register('groupOrg')} />
                            <label htmlFor="groupOrg">Группа организаций</label>
                        </div>
                    </div>
                </div>

                {/* РАСПОЛОЖЕНИЕ */}
                <div className={s.formItem}>
                    <label htmlFor="users" className={s.label}>
                        Расположение*
                    </label>
                    <div className={s.inputCheckbox}>
                        <div>
                            <input type="checkbox" id="popular" {...register('popular')} />
                            <label htmlFor="popular">Популярное</label>
                        </div>
                        <div>
                            <input type="checkbox" id="main" {...register('main')} />
                            <label htmlFor="main">Главное</label>
                        </div>
                        <div>
                            <input type="checkbox" id="useful" {...register('useful')} />
                            <label htmlFor="useful">Полезное</label>
                        </div>
                        <div>
                            <input type="checkbox" id="news" {...register('news')} />
                            <label htmlFor="news">Новости</label>
                        </div>
                    </div>
                </div>

                {/* ДАТА ПУБЛИКАЦИИ */}
                {errors.publicationDate && (
                    <span className={s.errorMsg}>Дата и время должны быть не раньше {parseData(minDate)}</span>
                )}
                <div className={s.formItem}>
                    <label htmlFor="datetime" className={s.label}>
                        Дата публикации*
                    </label>
                    <input
                        className={s.input}
                        type="datetime-local"
                        placeholder={'dsfgvdsfgg'}
                        {...register('publicationDate', {
                            required: false,
                            min: minDate,
                        })}
                    />
                </div>

                {/* ОПИСАНИЕ */}
                <div>
                    <textarea className={s.textarea} {...register('description')} placeholder="Описание" />
                </div>
            </form>

            {!preview && <EditorNews setContent={setContent} content={content} />}
            {preview && <Preview content={content} />}
        </div>
    );
};

export default CreateNews;

const customStyles: StylesConfig<Option, true, GroupBase<Option>> = {
    control: (baseStyles) => ({ ...baseStyles, borderColor: 'transparent' }),
    container: (baseStyles) => ({ ...baseStyles, flex: '1' }),
    placeholder: (baseStyles) => ({ ...baseStyles, padding: '0 8px' }),
    indicatorsContainer: () => ({ display: 'none' }),
};

const customTheme = (theme: any) => ({
    ...theme,
    colors: {
        ...theme.colors,
        primary: 'transparent',
        primary50: '#BF561E',
        primary25: '#FC7630',
        neutral0: '#F6F6F6',
        neutral30: 'transparent',
        neutral40: 'transparent',
    },
});
