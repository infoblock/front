import React, { FC, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { AiOutlineDelete, AiOutlineLink } from 'react-icons/ai';
import { BiPencil } from 'react-icons/bi';
import { BsThreeDots } from 'react-icons/bs';
import { FormDataResponse } from '../../../../constants/types';
import { parseData } from '../../../../helpers/parseDate';
import { translateStrings } from '../../../../helpers/translateLocationAndAudience';
import { NewsType } from '../../../../constants/enums';
import { useOutsideClick } from '../../../../shared/hooks';
import DeletePublicationPopup from '../../../deletePopup/delete';
import ToolTipComponent from '../../../ui-kit/tooltip/tooltip';
import s from './cardNews.module.css';

const CardNews: FC<{ n: FormDataResponse; newsType: NewsType }> = ({ n, newsType }) => {
    const [dots, setDots] = useState(false);
    const refShowEditDel = useOutsideClick(() => setDots(false));
    const navigate = useNavigate();

    const openItemNews = (news: FormDataResponse) => {
        news.link ? window.open(news.link, '_blank') : navigate(`/publications/${news.id}`);
    };

    const [isOpenDelete, setIsOpenDelete] = useState(false);

    return (
        <>
            <tr className={s.items} key={n.id}>
                <td onClick={() => openItemNews(n)}>
                    <div className={s.newsItemContent}>
                        <div className={s.newsItemImage} style={{ backgroundImage: `url(${n.avatar})` }}>
                            {n.link ? (
                                <AiOutlineLink className={s.newsItemIcon} />
                            ) : (
                                <BiPencil className={s.newsItemIcon} />
                            )}
                        </div>
                        <div className={s.newsItemText}>
                            <div className={s.newsItemTextTitle}>{n.title}</div>
                            <div className={s.newItemTextDescription}>{n.description}</div>
                        </div>
                    </div>
                </td>
                <td onClick={() => openItemNews(n)}>
                    {newsType === NewsType.Draft ? (
                        <div className={s.newsItemDate}>{parseData(n.createDate)}</div>
                    ) : (
                        <div className={s.newsItemDate}>{parseData(n.publicationDate)}</div>
                    )}
                </td>
                <td onClick={() => openItemNews(n)}>
                    <div className={s.newsItemTag}>
                        <ToolTipComponent tooltipContent={n.tags}>
                            <>
                                <div>{n.tags[0] || '—'}</div>
                                <div className={s.newsItemTagCount}>{n.tags.length > 1 && `+${n.tags.length - 1}`}</div>
                            </>
                        </ToolTipComponent>
                    </div>
                </td>
                <td onClick={() => openItemNews(n)}>
                    <div className={s.newsItemViews}>
                        <ToolTipComponent tooltipContent={translateStrings(n.locations)}>
                            <>
                                <div>{translateStrings(n.locations)[0] || '—'}</div>
                                <div className={s.newsItemTagCount}>
                                    {n.locations.length > 1 && `+${n.locations.length - 1}`}
                                </div>
                            </>
                        </ToolTipComponent>
                    </div>
                </td>
                {newsType === NewsType.Published && (
                    <td onClick={() => openItemNews(n)}>
                        <div className={s.newsItemDemonstration}>{n.viewsCount}</div>
                    </td>
                )}

                <td>
                    <div className={s.dots} onClick={() => setDots(!dots)}>
                        <BsThreeDots />
                        {dots && (
                            <div ref={refShowEditDel} className={s.editDel}>
                                <div onClick={() => navigate(`/edit/${n.id}`)}>
                                    <BiPencil className={s.newsItemEdit} />
                                    <span>Редактировать</span>
                                </div>
                                <div onClick={() => setIsOpenDelete(true)} className={s.newsItemEllipsis}>
                                    <AiOutlineDelete />
                                    <span>Удалить</span>
                                </div>
                            </div>
                        )}
                    </div>
                </td>
            </tr>

            <DeletePublicationPopup id={n.id} isOpenDelete={isOpenDelete} setIsOpenDelete={setIsOpenDelete} />
        </>
    );
};

export default CardNews;
