import React, { FC } from 'react';
import { NavLink, Outlet } from 'react-router-dom';
import cn from 'classnames';
import { useAppDispatch } from '../../../../shared/hooks';
import { setQuery } from '../../../../store/slices/searchSlice';
import { getNewsThunk } from '../../../../store/slices/newsSlice';
import MySelect from '../../../ui-kit/select/select';
import s from './layoutNews.module.css';

const LayoutNews: FC = () => {
    const setActive = ({ isActive }: { isActive: boolean }): string =>
        isActive ? cn(s.newsStatusActive, s.newsStatus) : s.newsStatus;
    const dispatch = useAppDispatch();

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        dispatch(setQuery(event.target.value));
        dispatch(getNewsThunk());
    };

    return (
        <div className={s.mainContent}>
            <div className={s.heading}>
                <div className={s.newsStatuses}>
                    <NavLink to="published" className={setActive}>
                        Опубликованные
                    </NavLink>
                    <NavLink to="deferred" className={setActive}>
                        Ожидают публикации
                    </NavLink>
                    <NavLink to="draft" className={setActive}>
                        Черновики
                    </NavLink>
                </div>
                <div className={s.newsFilters}>
                    <input
                        className={s.searchInput}
                        onChange={handleInputChange}
                        type="text"
                        placeholder={'Поиск новости'}
                    />
                    <MySelect />
                </div>
            </div>

            <Outlet />
        </div>
    );
};

export default LayoutNews;
