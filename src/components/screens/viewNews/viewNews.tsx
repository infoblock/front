import React, { FC, useEffect, useState } from 'react';
import parse from 'html-react-parser';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { AiOutlineEye } from 'react-icons/ai';
import { BsArrowLeft } from 'react-icons/bs';
import { useAppDispatch, useAppSelector } from '../../../shared/hooks';
import { getNewsItemThunk } from '../../../store/slices/newsSlice';
import { parseData } from '../../../helpers/parseDate';
import { translateStrings } from '../../../helpers/translateLocationAndAudience';
import DeletePublicationPopup from '../../deletePopup/delete';
import s from './viewNews.module.css';

const ViewNews: FC = () => {
    const { id } = useParams();
    const navigate = useNavigate();
    const dispatch = useAppDispatch();
    const news = useAppSelector((state) => state.news.item);

    useEffect(() => {
        dispatch(getNewsItemThunk(id));
    }, []);

    const editPublication = () => {
        navigate(`/edit/${id}`);
    };

    const [isOpenDelete, setIsOpenDelete] = useState(false);

    return (
        <div className={s.content}>
            <div className={s.headButtons}>
                <div className={s.btns}>
                    <Link to={'/'} className={s.btnDefault}>
                        <BsArrowLeft style={{ transform: 'translateY(2px)' }} /> Назад
                    </Link>
                </div>

                <div className={s.btns}>
                    <button onClick={() => setIsOpenDelete(true)} className={s.btnDelete}>
                        Удалить
                    </button>

                    <button onClick={() => editPublication()} className={s.btnCreate}>
                        Редактировать
                    </button>
                </div>
                <DeletePublicationPopup id={id} isOpenDelete={isOpenDelete} setIsOpenDelete={setIsOpenDelete} />
            </div>
            {news && (
                <div>
                    <h1>{news.title}</h1>
                    <div className={s.descriptions}>
                        <div>{parseData(news.publicationDate)}</div>
                        <div className={s.viewsCount}>
                            <AiOutlineEye style={{ fontSize: '22px' }} /> {news.viewsCount}
                        </div>
                    </div>
                    <div className={s.editDate}>{news.editeDate && `Изменено ${parseData(news.editeDate)}`}</div>
                    <div className={s.tags}>
                        {news.tags.map((t) => (
                            <div className={s.tag}>{t}</div>
                        ))}
                    </div>
                    {news.body && parse(news.body)}
                    <h1 className={s.author}>{news.author && `Автор: ${news.author}`}</h1>
                    <div>
                        <div>
                            Аудитория:{' '}
                            <div className={s.tags}>
                                {translateStrings(news.audienceTypes).map((a, i) => (
                                    <div className={s.tag} key={i}>
                                        {a}{' '}
                                    </div>
                                ))}
                            </div>
                        </div>
                        <div>
                            Расположение:{' '}
                            <div className={s.tags}>
                                {translateStrings(news.locations).map((a, i) => (
                                    <div className={s.tag} key={i}>
                                        {a}{' '}
                                    </div>
                                ))}
                            </div>
                        </div>
                    </div>
                </div>
            )}
        </div>
    );
};

export default ViewNews;
