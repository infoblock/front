import React, { FC, useEffect, useState } from 'react';
import { BsArrowLeft } from 'react-icons/bs';
import CreatableSelect from 'react-select/creatable';
import { SubmitHandler, useForm } from 'react-hook-form';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { ActionMeta, GroupBase, MultiValue, StylesConfig } from 'react-select';
import { parseData, parseDataToUTC } from '../../../helpers/parseDate';
import { Content, FormData, Option } from '../../../constants/types';
import { parseCheckbox } from '../../../helpers/parseCheckbox';
import { useAppDispatch, useAppSelector } from '../../../shared/hooks';
import { deleteNewsItemThunk, getNewsItemThunk, patchNewsThunk } from '../../../store/slices/newsSlice';
import Preview from './preview';
import EditorNews from './editorNews';
import s from './editNews.module.css';

const options: Option[] = [
    { value: 'nalogs', label: 'налоги' },
    { value: 'sotrudniky', label: 'сотрудники' },
    { value: 'buxuchet', label: 'бухучет' },
    { value: 'prochayaOtchetnost', label: 'прочая отчетность' },
    { value: 'biznes', label: 'бизнес' },
    { value: 'instrumentyBuhgaltera', label: 'инструменты бухгалтера' },
];

const EditNews: FC = () => {
    const { id } = useParams();
    const navigate = useNavigate();
    const dispatch = useAppDispatch();
    const news = useAppSelector((state) => state.news.item);

    const [content, setContent] = useState<Content>({
        content: `${news?.body}`,
    });
    const [preview, setPreview] = useState(false);

    useEffect(() => {
        dispatch(getNewsItemThunk(id));
    }, []);

    const deletePublication = () => {
        dispatch(deleteNewsItemThunk(id));
        navigate('/');
    };

    const onSubmit: SubmitHandler<FormData> = (data) => {
        data.id = id;
        data.tags = tagLabels;
        const [audienceValues, locationValues] = parseCheckbox(data);
        data.audienceTypes = audienceValues;
        data.locations = locationValues;

        data.body = content.content;
        data.publicationDate = parseDataToUTC(data.publicationDate);

        dispatch(patchNewsThunk(data));
        navigate('/');
    };

    const [tagLabels, setTagLabels] = useState<string[]>([]);
    const {
        register,
        handleSubmit,
        formState: { errors },
        reset,
    } = useForm<FormData>({
        defaultValues: {
            title: news?.title,
            author: news?.author,
            avatar: news?.avatar,
            link: news?.link,
            featureFlag: news?.featureFlag,
            description: news?.description,
        },
    });

    useEffect(() => {
        news && reset(news);
    }, [news]);

    const currentDate = new Date();
    const minDate = currentDate.toString();

    const handleChange = (newValue: MultiValue<Option>, actionMeta: ActionMeta<Option>) => {
        const labels = newValue.map((v: Option) => v.label);
        setTagLabels(labels);
    };

    const defaultTags = news?.tags || [];
    const defaultOptionsArray: Option[] = defaultTags.map((tag: any) => ({
        value: tag,
        label: tag,
    }));

    return (
        <div className={s.wrapper}>
            <div className={s.headButtons}>
                <div className={s.btns}>
                    <Link to={'/'} className={s.btnDefault}>
                        <BsArrowLeft style={{ transform: 'translateY(2px)' }} /> Назад
                    </Link>
                    {news?.body && (
                        <button
                            className={s.btnDefault}
                            onClick={() => {
                                setPreview(!preview);
                            }}
                        >
                            {preview ? 'Редактировать' : 'Предпросмотр'}
                        </button>
                    )}
                </div>
                <div className={s.btns}>
                    <button onClick={() => deletePublication()} className={s.btnDelete}>
                        Удалить
                    </button>
                    {/*<button onClick={handleSubmit(saveToDraft)} className={s.btnDefault}>Снять с публикации</button>*/}
                    <button onClick={handleSubmit(onSubmit)} className={s.btnCreate}>
                        Сохранить изменения
                    </button>
                </div>
            </div>
            <form className={s.form}>
                {/* ЗАГОЛОВОК */}
                {errors.title && <span className={s.errorMsg}>Введите заголовок публикации</span>}
                <div className={s.formItem}>
                    <label htmlFor="title" className={s.label}>
                        Заголовок*
                    </label>
                    <input
                        {...register('title', { required: true })}
                        type="text"
                        id="title"
                        className={s.input}
                        placeholder="Введите заголовок"
                    />
                </div>

                {/* АВТОР */}
                {errors.author && <span className={s.errorMsg}>Введите автора публикации</span>}
                <div className={s.formItem}>
                    <label htmlFor="author" className={s.label}>
                        Автор статьи*
                    </label>
                    <input
                        {...register('author', { required: false })}
                        type="text"
                        id="author"
                        className={s.input}
                        placeholder="Введите имя автора"
                    />
                </div>

                {/* АВАТАР */}
                {errors.avatar && <span className={s.errorMsg}>Введите ссылку на публикацию</span>}
                <div className={s.formItem}>
                    <label htmlFor="avatar" className={s.label}>
                        Ссылка на фото
                    </label>
                    <input
                        {...register('avatar', { required: false })}
                        type="url"
                        id="avatar"
                        className={s.input}
                        placeholder="Введите ссылку"
                    />
                </div>

                {/* ССЫЛКА НА СТАТЬЮ */}
                {errors.avatar && <span className={s.errorMsg}>Введите ссылку на публикацию</span>}
                {news?.link && (
                    <div className={s.formItem}>
                        <label htmlFor="link" className={s.label}>
                            Ссылка на статью
                        </label>
                        <input
                            {...register('link', { required: true })}
                            type="url"
                            id="link"
                            className={s.input}
                            placeholder="Введите ссылку"
                        />
                    </div>
                )}

                {/* ТЕГ */}
                {errors.tags?.message}
                <div className={s.formItem}>
                    <label className={s.label}>Тег</label>
                    <CreatableSelect
                        isMulti
                        options={options}
                        placeholder={'Выберите тег'}
                        styles={customStyles}
                        theme={customTheme}
                        defaultValue={defaultOptionsArray}
                        {...register('tags')}
                        onChange={handleChange}
                    />
                </div>

                {/* АУДИТОРИЯ */}
                <div className={s.formItem}>
                    <label htmlFor="users" className={s.label}>
                        Аудитория*
                    </label>
                    <div className={s.inputCheckbox}>
                        <div>
                            <input
                                type="checkbox"
                                id="all"
                                {...register('all')}
                                defaultChecked={news?.audienceTypes.includes('all')}
                            />
                            <label htmlFor="all">Все</label>
                        </div>
                        <div>
                            <input
                                type="checkbox"
                                id="abonent"
                                {...register('abonent')}
                                defaultChecked={news?.audienceTypes.includes('abonent')}
                            />
                            <label htmlFor="abonent">Абонент</label>
                        </div>
                        <div>
                            <input
                                type="checkbox"
                                id="portUser"
                                {...register('portUser')}
                                defaultChecked={news?.audienceTypes.includes('portUser')}
                            />
                            <label htmlFor="portUser">Портальный пользователь</label>
                        </div>
                        <div>
                            <input
                                type="checkbox"
                                id="groupOrg"
                                {...register('groupOrg')}
                                defaultChecked={news?.audienceTypes.includes('groupOrg')}
                            />
                            <label htmlFor="groupOrg">Группа организаций</label>
                        </div>
                    </div>
                </div>

                {/* ФИЧАФЛАГ */}
                <div className={s.formItem}>
                    <label htmlFor="fichaflag" className={s.label}>
                        Фичафлаг
                    </label>
                    <input
                        type="text"
                        id="fichaflag"
                        {...register('featureFlag')}
                        className={s.input}
                        placeholder="Введите название фичафлага"
                    />
                </div>

                {/* РАСПОЛОЖЕНИЕ */}
                <div className={s.formItem}>
                    <label htmlFor="location" className={s.label}>
                        Расположение
                    </label>
                    <div className={s.inputCheckbox}>
                        <div>
                            <input
                                type="checkbox"
                                id="popular"
                                {...register('popular')}
                                defaultChecked={news?.locations.includes('popular')}
                            />
                            <label htmlFor="popular">Популярное</label>
                        </div>
                        <div>
                            <input
                                type="checkbox"
                                id="main"
                                {...register('main')}
                                defaultChecked={news?.locations.includes('main')}
                            />
                            <label htmlFor="main">Главное</label>
                        </div>
                        <div>
                            <input
                                type="checkbox"
                                id="useful"
                                {...register('useful')}
                                defaultChecked={news?.locations.includes('useful')}
                            />
                            <label htmlFor="useful">Полезное</label>
                        </div>
                        <div>
                            <input
                                type="checkbox"
                                id="news"
                                {...register('news')}
                                defaultChecked={news?.locations.includes('news')}
                            />
                            <label htmlFor="news">Новости</label>
                        </div>
                    </div>
                </div>
                {/* СТАТУС */}
                <div className={s.formItem}>
                    <label htmlFor="publish" className={s.label}>
                        Статус
                    </label>
                    <div className={s.inputCheckbox}>
                        <div>
                            <input
                                type="checkbox"
                                id="publish"
                                {...register('publish')}
                                defaultChecked={news?.isActive}
                            />
                            <label htmlFor="publish">Опубликовать</label>
                        </div>
                    </div>
                </div>

                {/* ДАТА ПУБЛИКАЦИИ */}
                {errors.publicationDate && (
                    <span className={s.errorMsg}>Дата и время должны быть не раньше {parseData(minDate)}</span>
                )}
                <div className={s.formItem}>
                    <label htmlFor="datetime" className={s.label}>
                        Дата публикации*
                    </label>
                    <input
                        className={s.input}
                        type="datetime-local"
                        placeholder={'dsfgvdsfgg'}
                        defaultValue={news?.publicationDate?.slice(0, -1)}
                        {...register('publicationDate', {
                            required: false,
                            min: minDate,
                        })}
                    />
                </div>

                {/* ОПИСАНИЕ */}
                <div>
                    <textarea className={s.textarea} {...register('description')} placeholder="Описание" />
                </div>
            </form>

            {news?.body && !preview && <EditorNews setContent={setContent} content={content} />}
            {news?.body && preview && <Preview content={content} />}
        </div>
    );
};

export default EditNews;

const customStyles: StylesConfig<Option, true, GroupBase<Option>> = {
    control: (baseStyles) => ({ ...baseStyles, borderColor: 'transparent' }),
    container: (baseStyles) => ({ ...baseStyles, flex: '1' }),
    placeholder: (baseStyles) => ({ ...baseStyles, padding: '0 8px' }),
    indicatorsContainer: () => ({ display: 'none' }),
};

const customTheme = (theme: any) => ({
    ...theme,
    colors: {
        ...theme.colors,
        primary: 'transparent',
        primary50: '#BF561E',
        primary25: '#FC7630',
        neutral0: '#F6F6F6',
        neutral30: 'transparent',
        neutral40: 'transparent',
    },
});
