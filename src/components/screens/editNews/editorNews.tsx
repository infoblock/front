import React from 'react';
import cn from 'classnames';
import { HiCodeBracket, HiOutlineListBullet } from 'react-icons/hi2';
import { VscListOrdered } from 'react-icons/vsc';
import { BiImages } from 'react-icons/bi';
import { MdOutlineVideoLibrary } from 'react-icons/md';
import { AiOutlineAlignCenter, AiOutlineAlignLeft, AiOutlineAlignRight } from 'react-icons/ai';
import { EditorContent, useEditor, FloatingMenu, Editor } from '@tiptap/react';
import StarterKit from '@tiptap/starter-kit';
import Image from '@tiptap/extension-image';
import Highlight from '@tiptap/extension-highlight';
import TextAlign from '@tiptap/extension-text-align';
import Underline from '@tiptap/extension-underline';
import Youtube from '@tiptap/extension-youtube';
import { EditorProps, EditorContentProps } from '../../../constants/types';
import s from './editNews.module.css';

const MenuBar = ({ editor }: EditorProps) => {
    if (!editor) {
        return null;
    }

    return (
        <div className={s.menuBar}>
            <button
                onClick={() => editor.chain().focus().toggleBold().run()}
                disabled={!editor.can().chain().focus().toggleBold().run()}
                className={cn(editor.isActive('bold') ? s.editBtnActive : '', s.editBtn)}
            >
                <b>B</b>
            </button>
            <button
                onClick={() => editor.chain().focus().toggleItalic().run()}
                disabled={!editor.can().chain().focus().toggleItalic().run()}
                className={cn(editor.isActive('italic') ? s.editBtnActive : '', s.editBtn)}
            >
                <i>I</i>
            </button>
            <button
                onClick={() => editor.chain().focus().toggleStrike().run()}
                disabled={!editor.can().chain().focus().toggleStrike().run()}
                className={cn(editor.isActive('strike') ? s.editBtnActive : '', s.editBtn)}
            >
                <s>S</s>
            </button>
            <button
                onClick={() => editor.chain().focus().toggleCode().run()}
                disabled={!editor.can().chain().focus().toggleCode().run()}
                className={cn(editor.isActive('code') ? s.editBtnActive : '', s.editBtn)}
            >
                code
            </button>
            <button
                onClick={() => editor.chain().focus().toggleHighlight({ color: '#ffc078' }).run()}
                className={cn(editor.isActive('highlight', { color: '#ffc078' }) ? s.editBtnActive : '', s.editBtn)}
            >
                Выделение
            </button>
            <button
                onClick={() => editor.chain().focus().setTextAlign('left').run()}
                className={cn(editor.isActive({ textAlign: 'left' }) ? s.editBtnActive : '', s.editBtn)}
            >
                <AiOutlineAlignLeft />
            </button>
            <button
                onClick={() => editor.chain().focus().setTextAlign('center').run()}
                className={cn(editor.isActive({ textAlign: 'center' }) ? s.editBtnActive : '', s.editBtn)}
            >
                <AiOutlineAlignCenter />
            </button>
            <button
                onClick={() => editor.chain().focus().setTextAlign('right').run()}
                className={cn(editor.isActive({ textAlign: 'right' }) ? s.editBtnActive : '', s.editBtn)}
            >
                <AiOutlineAlignRight />
            </button>
            <button
                onClick={() => editor.chain().focus().toggleHeading({ level: 1 }).run()}
                className={cn(editor.isActive('heading', { level: 1 }) ? s.editBtnActive : '', s.editBtn)}
            >
                H1
            </button>
            <button
                onClick={() => editor.chain().focus().toggleHeading({ level: 2 }).run()}
                className={cn(editor.isActive('heading', { level: 2 }) ? s.editBtnActive : '', s.editBtn)}
            >
                H2
            </button>
            <button
                onClick={() => editor.chain().focus().toggleHeading({ level: 3 }).run()}
                className={cn(editor.isActive('heading', { level: 3 }) ? s.editBtnActive : '', s.editBtn)}
            >
                H3
            </button>
            <button
                onClick={() => editor.chain().focus().toggleHeading({ level: 4 }).run()}
                className={cn(editor.isActive('heading', { level: 4 }) ? s.editBtnActive : '', s.editBtn)}
            >
                H4
            </button>
            <button
                onClick={() => editor.chain().focus().toggleHeading({ level: 5 }).run()}
                className={cn(editor.isActive('heading', { level: 5 }) ? s.editBtnActive : '', s.editBtn)}
            >
                H5
            </button>
            <button
                onClick={() => editor.chain().focus().toggleHeading({ level: 6 }).run()}
                className={cn(editor.isActive('heading', { level: 6 }) ? s.editBtnActive : '', s.editBtn)}
            >
                H6
            </button>
            <button
                onClick={() => editor.chain().focus().toggleBulletList().run()}
                className={cn(editor.isActive('bulletList') ? s.editBtnActive : '', s.editBtn)}
            >
                <HiOutlineListBullet style={{ fontSize: '20px' }} />
            </button>
            <button
                onClick={() => editor.chain().focus().toggleOrderedList().run()}
                className={cn(editor.isActive('orderedList') ? s.editBtnActive : '', s.editBtn)}
            >
                <VscListOrdered style={{ fontSize: '20px' }} />
            </button>
            <button
                onClick={() => editor.chain().focus().toggleCodeBlock().run()}
                className={cn(editor.isActive('codeBlock') ? s.editBtnActive : '', s.editBtn)}
            >
                Кодблок
            </button>
            <button
                onClick={() => editor.chain().focus().toggleBlockquote().run()}
                className={cn(editor.isActive('blockquote') ? s.editBtnActive : '', s.editBtn)}
            >
                Цитата
            </button>
            <button onClick={() => editor.chain().focus().setHorizontalRule().run()} className={s.editBtn}>
                Черта
            </button>
            <button onClick={() => editor.chain().focus().setHardBreak().run()} className={s.editBtn}>
                Разрыв
            </button>
            <button onClick={() => editor.chain().focus().unsetAllMarks().run()} className={s.editBtn}>
                clear marks
            </button>
            <button onClick={() => editor.chain().focus().clearNodes().run()} className={s.editBtn}>
                clear nodes
            </button>
        </div>
    );
};

const EditorNews: React.FC<EditorContentProps> = ({ setContent, content }) => {
    const editor: Editor | null = useEditor({
        extensions: [
            StarterKit,
            Image.configure({ inline: true, allowBase64: true }),
            TextAlign.configure({ types: ['heading', 'paragraph'] }),
            Highlight.configure({ multicolor: true }),
            Underline,
            Youtube.configure({
                inline: false,
            }),
        ],
        autofocus: 'start',
        content: content.content,
        onBlur: ({ editor }) => {
            setContent({ content: editor.getHTML() });
        },
    });

    const addImage = () => {
        const url = window.prompt('Вставьте URL картинки');
        if (url) {
            editor?.chain().focus().setImage({ src: url }).run();
        }
    };
    const addYoutubeVideo = () => {
        const url = prompt('Вставьте URL из YouTube');

        if (url) {
            editor?.commands.setYoutubeVideo({
                src: url,
                width: window.innerWidth * 0.8,
                height: 480,
            });
        }
    };

    return (
        <>
            <div>
                <MenuBar editor={editor} />
                <EditorContent editor={editor} className={s.contentEditor} />
                {editor && (
                    <FloatingMenu editor={editor} tippyOptions={{ duration: 100 }}>
                        <div className={s.floatingMenu}>
                            <div className={s.floatingMenuBlock}>
                                <div className={s.blockTitle}>Заголовок</div>
                                <div className={s.block}>
                                    <button
                                        className={cn(
                                            editor.isActive('heading', {
                                                level: 1,
                                            })
                                                ? s.blockButtonActive
                                                : '',
                                            s.blockButton
                                        )}
                                        onClick={() => editor.chain().focus().toggleHeading({ level: 1 }).run()}
                                    >
                                        <span>H1</span>Заголовок 1
                                    </button>
                                    <button
                                        className={cn(
                                            editor.isActive('heading', {
                                                level: 2,
                                            })
                                                ? s.blockButtonActive
                                                : '',
                                            s.blockButton
                                        )}
                                        onClick={() => editor.chain().focus().toggleHeading({ level: 2 }).run()}
                                    >
                                        <span>H2</span>Заголовок 2
                                    </button>
                                    <button
                                        className={cn(
                                            editor.isActive('heading', {
                                                level: 3,
                                            })
                                                ? s.blockButtonActive
                                                : '',
                                            s.blockButton
                                        )}
                                        onClick={() => editor.chain().focus().toggleHeading({ level: 3 }).run()}
                                    >
                                        <span>H3</span>Заголовок 3
                                    </button>
                                </div>
                            </div>
                            <div className={s.floatingMenuBlock}>
                                <div className={s.blockTitle}>Текст</div>
                                <button
                                    onClick={() => editor.chain().focus().setParagraph().run()}
                                    className={cn(
                                        editor.isActive('paragraph') ? s.blockButtonActive : '',
                                        s.blockButton
                                    )}
                                >
                                    <span>T</span>Обычный
                                </button>
                                <button
                                    onClick={() => editor.chain().focus().toggleBold().run()}
                                    disabled={!editor.can().chain().focus().toggleBold().run()}
                                    className={cn(editor.isActive('bold') ? s.blockButtonActive : '', s.blockButton)}
                                >
                                    <b>B</b>Жирный
                                </button>
                                <button
                                    onClick={() => editor.chain().focus().toggleItalic().run()}
                                    disabled={!editor.can().chain().focus().toggleItalic().run()}
                                    className={cn(editor.isActive('italic') ? s.blockButtonActive : '', s.blockButton)}
                                >
                                    <i>I</i>Курсивный
                                </button>
                                <button
                                    onClick={() => editor.chain().focus().toggleStrike().run()}
                                    disabled={!editor.can().chain().focus().toggleStrike().run()}
                                    className={cn(editor.isActive('strike') ? s.blockButtonActive : '', s.blockButton)}
                                >
                                    <s>S</s>Зачеркнутый
                                </button>
                                <button
                                    onClick={() => editor.chain().focus().toggleUnderline().run()}
                                    disabled={!editor.can().chain().focus().toggleUnderline().run()}
                                    className={cn(
                                        editor.isActive('underline') ? s.blockButtonActive : '',
                                        s.blockButton
                                    )}
                                >
                                    <u>U</u>Подчеркнутый
                                </button>
                                <button
                                    onClick={() => editor.chain().focus().toggleCode().run()}
                                    disabled={!editor.can().chain().focus().toggleCode().run()}
                                    className={cn(editor.isActive('code') ? s.blockButtonActive : '', s.blockButton)}
                                >
                                    <HiCodeBracket className={s.blockButtonIcon} /> Моноширный
                                </button>
                            </div>
                            <div className={s.floatingMenuBlock}>
                                <div className={s.blockTitle}>Список</div>
                                <button
                                    onClick={() => editor.chain().focus().toggleBulletList().run()}
                                    className={cn(
                                        editor.isActive('bulletList') ? s.blockButtonActive : '',
                                        s.blockButton
                                    )}
                                >
                                    <HiOutlineListBullet className={s.blockButtonIcon} />
                                    Маркированный
                                </button>
                                <button
                                    onClick={() => editor.chain().focus().toggleOrderedList().run()}
                                    className={cn(
                                        editor.isActive('orderedList') ? s.blockButtonActive : '',
                                        s.blockButton
                                    )}
                                >
                                    <VscListOrdered className={s.blockButtonIcon} />
                                    Нумерованный
                                </button>
                            </div>
                            <div className={s.floatingMenuBlock}>
                                <div className={s.blockTitle}>Мултимедиа</div>
                                <button className={s.blockButton} onClick={addImage}>
                                    <BiImages className={s.blockButtonIcon} />
                                    Изображение
                                </button>
                                <button className={s.blockButton} onClick={addYoutubeVideo} disabled>
                                    <MdOutlineVideoLibrary className={s.blockButtonIcon} />
                                    Видео
                                </button>
                            </div>
                        </div>
                    </FloatingMenu>
                )}
            </div>
        </>
    );
};

export default EditorNews;
