import React from 'react';
import parse from 'html-react-parser';
import { ContentContent } from '../../../constants/types';

const Preview = ({ content }: ContentContent) => {
    return <div>{parse(content.content)}</div>;
};

export default Preview;
