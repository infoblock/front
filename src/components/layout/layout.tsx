import React, { FC } from 'react';
import { HiOutlineDuplicate, HiOutlineUser } from 'react-icons/hi';
import { NavLink, Outlet } from 'react-router-dom';
import { BsBarChart } from 'react-icons/bs';
import cn from 'classnames';
import s from './layout.module.css';

const Layout: FC = () => {
    const setActive = ({ isActive }: { isActive: boolean }): string =>
        isActive ? cn(s.navbarItem, s.active) : s.navbarItem;

    return (
        <>
            <div className={s.content}>
                <div className={s.name}>
                    <div>Контур</div>
                    <div className={s.nameExtern}>Экстерн</div>
                </div>
                <div className={s.main}>
                    <div className={s.navbar}>
                        <NavLink to="publications" className={setActive}>
                            <HiOutlineDuplicate className={s.navbarIcon} />
                            <span>Публикации</span>
                        </NavLink>
                        <NavLink to="/statistics" className={setActive}>
                            <BsBarChart className={s.navbarIcon} />
                            <span>Статистика</span>
                        </NavLink>
                        <NavLink to="/editors" className={setActive}>
                            <HiOutlineUser className={s.navbarIcon} />
                            <span>Редакторы</span>
                        </NavLink>
                    </div>

                    <div className={s.outlet}>
                        <Outlet />
                    </div>
                </div>
            </div>
        </>
    );
};

export default Layout;
