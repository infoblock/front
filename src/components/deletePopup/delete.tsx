import React from 'react';
import { useNavigate } from 'react-router-dom';
import { deleteNewsItemThunk } from '../../store/slices/newsSlice';
import { useAppDispatch, useOutsideClick } from '../../shared/hooks';
import { DeletePublicationProps } from '../../constants/types';
import Popup from '../ui-kit/pop-up/popup';
import s from './delete.module.css';

const DeletePublicationPopup: React.FC<DeletePublicationProps> = ({ id, isOpenDelete, setIsOpenDelete }) => {
    const refShowPopup = useOutsideClick(() => setIsOpenDelete(false));
    const navigate = useNavigate();
    const dispatch = useAppDispatch();

    const deletePublication = () => {
        id && dispatch(deleteNewsItemThunk(id));
        navigate('/');
    };

    return (
        <Popup isOpened={isOpenDelete}>
            <div ref={refShowPopup} className={s.deletePopup}>
                <div>Вы уверены, что хотите удалить этот пост?</div>
                <div className={s.btns}>
                    <button className={s.btnDefault} onClick={() => setIsOpenDelete(false)}>
                        Отмена
                    </button>
                    <button className={s.btnDelete} onClick={() => deletePublication()}>
                        Удалить
                    </button>
                </div>
            </div>
        </Popup>
    );
};

export default DeletePublicationPopup;
