import { FC, useState } from 'react';
import cn from 'classnames';
import { useForm } from 'react-hook-form';
import CreatableSelect from 'react-select/creatable';
import { MultiValue, ActionMeta, GroupBase, StylesConfig } from 'react-select';
import { parseCheckbox } from '../../../helpers/parseCheckbox';
import { FormData, Option } from '../../../constants/types';
import { parseData, parseDataToUTC } from '../../../helpers/parseDate';
import { postNewsThunk } from '../../../store/slices/newsSlice';
import { useAppDispatch } from '../../../shared/hooks';
import s from './linkForm.module.css';

const options: Option[] = [
    { value: 'nalogs', label: 'налоги' },
    { value: 'sotrudniky', label: 'сотрудники' },
    { value: 'buxuchet', label: 'бухучет' },
    { value: 'prochayaOtchetnost', label: 'прочая отчетность' },
    { value: 'biznes', label: 'бизнес' },
    { value: 'instrumentyBuhgaltera', label: 'инструменты бухгалтера' },
];

const LinkForm: FC<{ onCancel: () => void }> = ({ onCancel }) => {
    const [tagLabels, setTagLabels] = useState<string[]>([]);
    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm<FormData>();
    const dispatch = useAppDispatch();

    const onSubmit = handleSubmit((data) => {
        data.tags = tagLabels;
        const [audienceValues, locationValues] = parseCheckbox(data);
        data.audienceTypes = audienceValues;
        data.locations = locationValues;
        data.publish = true;
        data.publicationDate = parseDataToUTC(data.publicationDate);

        dispatch(postNewsThunk(data));
        onCancel();
    });

    const handleChange = (newValue: MultiValue<Option>, actionMeta: ActionMeta<Option>) => {
        const labels = newValue.map((v: Option) => v.label);
        setTagLabels(labels);
    };

    const currentDate = new Date();
    const minDate = currentDate.toString();

    return (
        <>
            <form className={s.form} onSubmit={onSubmit}>
                {/* ЗАГОЛОВОК */}
                {errors.title && <span className={s.errorMsg}>Введите заголовок публикации</span>}
                <div className={s.formItem}>
                    <label htmlFor="title" className={s.label}>
                        Заголовок*
                    </label>
                    <input
                        {...register('title', { required: true })}
                        type="text"
                        id="title"
                        className={s.input}
                        placeholder="Введите заголовок"
                    />
                </div>

                {/* ССЫЛКА НА ПУБЛИКАЦИЮ */}
                {errors.link && <span className={s.errorMsg}>Введите ссылку на публикацию</span>}
                <div className={s.formItem}>
                    <label htmlFor="link" className={s.label}>
                        Ссылка на статью*
                    </label>
                    <input
                        {...register('link', { required: true })}
                        type="url"
                        id="link"
                        className={s.input}
                        placeholder="Введите ссылку"
                    />
                </div>

                {/* АВАТАР */}
                {errors.avatar && <span className={s.errorMsg}>Введите ссылку на публикацию</span>}
                <div className={s.formItem}>
                    <label htmlFor="avatar" className={s.label}>
                        Ссылка на фото
                    </label>
                    <input
                        {...register('avatar')}
                        type="url"
                        id="avatar"
                        className={s.input}
                        placeholder="Введите ссылку"
                    />
                </div>

                {/* ТЕГ */}
                {errors.tags?.message}
                <div className={s.formItem}>
                    <label className={s.label}>Тег</label>
                    <CreatableSelect
                        isMulti
                        options={options}
                        placeholder={'Выберите тег'}
                        styles={customStyles}
                        theme={customTheme}
                        {...register('tags')}
                        onChange={handleChange}
                    />
                </div>

                {/* ФИЧАФЛАГ */}
                <div className={s.formItem}>
                    <label htmlFor="fichaflag" className={s.label}>
                        Фичафлаг
                    </label>
                    <input
                        type="text"
                        id="fichaflag"
                        {...register('featureFlag')}
                        className={s.input}
                        placeholder="Введите название фичафлага"
                    />
                </div>

                {/* АУДИТОРИЯ */}
                <div className={s.formItem}>
                    <label htmlFor="users" className={s.label}>
                        Аудитория*
                    </label>
                    <div className={s.inputCheckbox}>
                        <div>
                            <input type="checkbox" id="all" {...register('all')} defaultChecked={true} />
                            <label htmlFor="all">Все</label>
                        </div>
                        <div>
                            <input type="checkbox" id="abonent" {...register('abonent')} />
                            <label htmlFor="abonent">Абонент</label>
                        </div>
                        <div>
                            <input type="checkbox" id="portUser" {...register('portUser')} />
                            <label htmlFor="portUser">Портальный пользователь</label>
                        </div>
                        <div>
                            <input type="checkbox" id="groupOrg" {...register('groupOrg')} />
                            <label htmlFor="groupOrg">Группа организаций</label>
                        </div>
                    </div>
                </div>

                {/* РАСПОЛОЖЕНИЕ */}
                <div className={s.formItem}>
                    <label htmlFor="users" className={s.label}>
                        Расположение*
                    </label>
                    <div className={s.inputCheckbox}>
                        <div>
                            <input type="checkbox" id="popular" {...register('popular')} />
                            <label htmlFor="popular">Популярное</label>
                        </div>
                        <div>
                            <input type="checkbox" id="main" {...register('main')} />
                            <label htmlFor="main">Главное</label>
                        </div>
                        <div>
                            <input type="checkbox" id="useful" {...register('useful')} />
                            <label htmlFor="useful">Полезное</label>
                        </div>
                        <div>
                            <input type="checkbox" id="news" {...register('news')} />
                            <label htmlFor="news">Новости</label>
                        </div>
                    </div>
                </div>

                {/* ДАТА ПУБЛИКАЦИИ */}
                {errors.publicationDate && (
                    <span className={s.errorMsg}>Дата и время должны быть не раньше {parseData(minDate)}</span>
                )}
                <div className={s.formItem}>
                    <label htmlFor="datetime" className={s.label}>
                        Дата публикации*
                    </label>
                    <input
                        className={s.input}
                        type="datetime-local"
                        {...register('publicationDate', {
                            required: false,
                            min: minDate,
                        })}
                    />
                </div>

                {/* ОПИСАНИЕ */}
                <div>
                    <textarea className={s.textarea} {...register('description')} placeholder="Описание" />
                </div>

                <div className={s.btns}>
                    <button className={cn(s.btn)} type="submit">
                        Опубликовать
                    </button>
                    <div className={cn(s.btn, s.btnCanel)} onClick={onCancel}>
                        Отменить
                    </div>
                </div>
            </form>
        </>
    );
};

export default LinkForm;

const customStyles: StylesConfig<Option, true, GroupBase<Option>> = {
    control: (baseStyles) => ({ ...baseStyles, borderColor: 'transparent' }),
    container: (baseStyles) => ({ ...baseStyles, flex: '1' }),
    placeholder: (baseStyles) => ({ ...baseStyles, padding: '0 8px' }),
    indicatorsContainer: () => ({ display: 'none' }),
};

const customTheme = (theme: any) => ({
    ...theme,
    colors: {
        ...theme.colors,
        primary: 'transparent',
        primary50: '#BF561E',
        primary25: '#FC7630',
        neutral0: '#F6F6F6',
        neutral30: 'transparent',
        neutral40: 'transparent',
    },
});
