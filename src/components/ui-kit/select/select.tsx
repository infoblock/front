import React, { FC, useState } from 'react';
import { AiOutlinePlus } from 'react-icons/ai';
import { RiPencilLine } from 'react-icons/ri';
import { FiPaperclip } from 'react-icons/fi';
import { Link } from 'react-router-dom';
import { useOutsideClick } from '../../../shared/hooks';
import Popup from '../pop-up/popup';
import LinkForm from '../../forms/linkForm/linkForm';
import s from './select.module.css';

const MySelect: FC = () => {
    const [isShowSelect, setShowSelect] = useState(false);
    const refSelect = useOutsideClick(() => setShowSelect(false));

    const [isShowCreateLinkNews, setShowCreateLinkNews] = useState(false);
    const refShowCreateLinkNews = useOutsideClick(() => setShowCreateLinkNews(false));

    return (
        <div className={s.selectGroup} ref={refSelect}>
            <div className={s.createNewsButton} onClick={() => setShowSelect(!isShowSelect)}>
                <span>Создать</span>
                <AiOutlinePlus />
            </div>
            <Popup isOpened={isShowCreateLinkNews}>
                <div ref={refShowCreateLinkNews}>
                    <LinkForm onCancel={() => setShowCreateLinkNews(false)} />
                </div>
            </Popup>
            {isShowSelect && (
                <div className={s.options}>
                    <div
                        className={s.option}
                        onClick={() => {
                            setShowSelect(false);
                            setShowCreateLinkNews(true);
                        }}
                    >
                        <div className={s.optionIcon}>
                            <FiPaperclip />
                        </div>
                        По ссылке
                    </div>
                    <Link to="/create" className={s.option}>
                        <div className={s.optionIcon}>
                            <RiPencilLine />
                        </div>
                        Вручную
                    </Link>
                </div>
            )}
        </div>
    );
};

export default MySelect;
