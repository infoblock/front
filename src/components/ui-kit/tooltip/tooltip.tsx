import React, { ReactElement, useRef, useState } from 'react';
import s from './tooltip.module.css';

type PropsType = {
    children: ReactElement;
    tooltipContent: string[];
};

const ToolTipComponent: React.FC<PropsType> = ({ children, tooltipContent }) => {
    const refSetTimeout = useRef<NodeJS.Timeout>();
    const [showToolTip, setShowToolTip] = useState(false);

    const onMouseEnterHandler = () => {
        if (tooltipContent.length > 1) {
            refSetTimeout.current = setTimeout(() => {
                setShowToolTip(true);
            }, 750);
        }
    };

    const onMouseLeaveHandler = () => {
        clearTimeout(refSetTimeout.current);
        setShowToolTip(false);
    };

    return (
        <div className={s.container} onMouseEnter={onMouseEnterHandler} onMouseLeave={onMouseLeaveHandler}>
            {children}
            {showToolTip && (
                <div className={s.tooltip}>
                    {tooltipContent.map((t) => (
                        <div>{t}</div>
                    ))}
                </div>
            )}
        </div>
    );
};

export default ToolTipComponent;
