import React, { FC } from 'react';
import Portal from '../portal/portal';
import s from './popup.module.css';

const Popup: FC<{ children: React.ReactNode; isOpened: boolean }> = ({ children, isOpened }) => {
    if (!isOpened) return null;

    return (
        <Portal>
            <div className={s.popup} role="dialog">
                <div className="content">{children}</div>
            </div>
        </Portal>
    );
};

export default Popup;
