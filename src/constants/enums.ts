export enum NewsType {
    Published = 1,
    NotPublished = 2,
    Draft = 3,
}

export enum SortDirection {
    Ascending = 1,
    Descending = -1,
}

export enum SortField {
    ReadTime,
    CreateDate,
    PublicationDate,
    EditeDate,
}
