import { Editor } from '@tiptap/react';
import React from 'react';
import { NewsType, SortDirection, SortField } from './enums';

interface BaseFormData {
    id?: string;
    title: string;
    link: string;
    avatar: string;
    body: string;
    author: string;
    tags: string[];
    audienceTypes: string[];
    locations: string[];
    isActive: boolean;
    publicationDate: string | null;
    publish: boolean;
}

export interface FormData extends BaseFormData {
    all: boolean;
    abonent: boolean;
    portUser: boolean;
    groupOrg: boolean;
    featureFlag: string;
    popular: boolean;
    main: boolean;
    useful: boolean;
    news: boolean;
    description: string;
}

export interface FormDataResponse extends BaseFormData {
    description: string;
    featureFlag: string;
    previewsCount: number;
    viewsCount: number;
    readTime: number;
    createDate: string;
    editeDate: string | null;
}

export interface StateNews {
    foundEntities: FormDataResponse[];
    totalCount: number;
    item: FormDataResponse | null;
}

export interface EditorResponse {
    id: string;
    email: string;
    createDate: string;
    publicationsCount: number;
    enabled: boolean;
}

export interface StateEditors {
    foundEntities: EditorResponse[];
    totalCount: number;
    errorMessage: string | null;
    isSuccess?: boolean;
}

export interface Auth {
    email: string;
    password: string;
    passwordRepeat?: string;
}

export interface AuthState {
    email: string;
    password?: string;
    token?: string;
    accessToken?: string;
    refreshToken?: string;
    isAuth?: boolean;
    error?: string;
}

export interface StateSearch {
    query: string | null;
    author: string | null;
    tags: string[] | null;
    audienceTypes: string[] | null;
    location: string[] | null;
    newsType: NewsType | null;
    page: number | null;
    count: number | null;
    sortField: SortField | null;
    sortDirection: SortDirection | null;
}

export interface Content {
    content: string;
}

export interface Option {
    value: string;
    label: string;
}

export interface ContentContent {
    content: {
        content: string;
    };
}

export interface EditorProps {
    editor: Editor | null;
}

export interface EditorContentProps {
    setContent: React.Dispatch<React.SetStateAction<Content>>;
    content: {
        content: string;
    };
}

export interface InviteEditorsFormData {
    email: string;
}

export interface DeletePublicationProps {
    id?: string;
    isOpenDelete: boolean;
    setIsOpenDelete: React.Dispatch<React.SetStateAction<boolean>>;
}
