const translates: { [key: string]: string } = {
    popular: 'популярное',
    main: 'главное',
    useful: 'полезное',
    news: 'новости',
    all: 'все',
    abonent: 'абоненты',
    portUser: 'портальные пользователи',
    groupOrg: 'группа организаций',
};

export function translateStrings(strings: string[]): string[] {
    const translatedStrings: string[] = [];

    for (const str of strings) {
        if (translates.hasOwnProperty(str)) {
            translatedStrings.push(translates[str]);
        } else {
            translatedStrings.push(str);
        }
    }

    return translatedStrings;
}
