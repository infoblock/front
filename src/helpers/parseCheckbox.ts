import { FormData } from '../constants/types';

const ALL = 'all';
const ABONENT = 'abonent';
const PORT_USER = 'portUser';
const GROUP_ORG = 'groupOrg';
const POPULAR = 'popular';
const MAIN = 'main';
const USEFUL = 'useful';
const NEWS = 'news';

export function parseCheckbox(data: FormData): [string[], string[]] {
    const audienceValues: string[] = [];
    const locationValues: string[] = [];

    Object.entries(data).forEach(([key, value]) => {
        if ([ALL, ABONENT, PORT_USER, GROUP_ORG].includes(key)) {
            if (value === true) audienceValues.push(key);
        } else if ([POPULAR, MAIN, USEFUL, NEWS].includes(key)) {
            if (value === true) locationValues.push(key);
        }
    });

    return [audienceValues, locationValues];
}
