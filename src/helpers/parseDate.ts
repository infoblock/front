export function parseData(data: string | null): string {
    if (data === null) return '-';

    const inputDate = new Date(data);

    const formatter = new Intl.DateTimeFormat('ru', {
        day: '2-digit',
        month: '2-digit',
        year: 'numeric',
        hour: '2-digit',
        minute: '2-digit',
    });

    return formatter.format(inputDate).replace(/,/g, '');
}

export function parseDataToUTC(data: string | null): string | null {
    if (data === '' || data === null) return null;

    const formattedDate = new Date(data).toUTCString();
    const date = new Date(formattedDate);

    const year = date.getUTCFullYear();
    const month = (date.getUTCMonth() + 1).toString().padStart(2, '0');
    const day = date.getUTCDate().toString().padStart(2, '0');
    const hours = date.getUTCHours().toString().padStart(2, '0');
    const minutes = date.getUTCMinutes().toString().padStart(2, '0');
    const seconds = date.getUTCSeconds().toString().padStart(2, '0');
    const milliseconds = date.getUTCMilliseconds().toString().padStart(3, '0');

    return `${year}-${month}-${day}T${hours}:${minutes}:${seconds}.${milliseconds}Z`;
}
