import React, { FC, useEffect, useState } from 'react';
import { IoLockClosedOutline, IoLockOpenOutline } from 'react-icons/io5';
import { AiOutlinePlus } from 'react-icons/ai';
import cn from 'classnames';
import { useForm } from 'react-hook-form';
import { useAppDispatch, useAppSelector, useOutsideClick } from '../../shared/hooks';
import {
    addEditorThunk,
    banEditorThunk,
    getEditorsThunk,
    setIsSuccess,
    unbanEditorThunk,
} from '../../store/slices/editorsSlice';
import { parseData } from '../../helpers/parseDate';
import { EditorResponse, InviteEditorsFormData } from '../../constants/types';
import Popup from '../../components/ui-kit/pop-up/popup';
import s from './editors.module.css';

const Editors: FC = () => {
    const [viewPopup, setViewPopup] = useState(false);
    const refPopup = useOutsideClick(() => setViewPopup(false));

    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm<InviteEditorsFormData>();

    const dispatch = useAppDispatch();
    const editors = useAppSelector((state) => state.editors);

    useEffect(() => {
        dispatch(getEditorsThunk());
    }, []);

    const onSubmit = handleSubmit((data) => {
        dispatch(addEditorThunk(data.email));
    });

    useEffect(() => {
        if (editors.isSuccess) {
            setViewPopup(false);
            dispatch(setIsSuccess(false));
        }
    }, [editors.isSuccess]);

    const changeStatusEditor = (editor: EditorResponse) => {
        editor.enabled ? dispatch(banEditorThunk(editor.id)) : dispatch(unbanEditorThunk(editor.id));
    };

    return (
        <>
            <div className={s.mainContent}>
                <div className={s.addNewsEditor} onClick={() => setViewPopup(true)}>
                    Добавить
                    <AiOutlinePlus />
                </div>

                <table className={s.table}>
                    <thead>
                        <tr className={s.titles}>
                            <th>Почта</th>
                            <th>Дата приглашения</th>
                            <th>Количесто статей</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {editors.foundEntities.map((e: EditorResponse) => (
                            <tr className={s.items} key={e.id}>
                                <td>
                                    <div className={s.editorEmail}>{e.email}</div>
                                </td>
                                <td>
                                    <div className={s.editorCreateDate}>{parseData(e.createDate)}</div>
                                </td>
                                <td>
                                    <div className={s.editorPublicationsCount}>{e.publicationsCount}</div>
                                </td>
                                <td>
                                    <div className={e.enabled ? s.editorIsActive : s.editorDontActive}>
                                        {e.enabled ? 'Активен' : 'Заблокирован'}
                                    </div>
                                </td>
                                <td onClick={() => changeStatusEditor(e)} style={{ cursor: 'pointer' }}>
                                    {e.enabled ? (
                                        <IoLockOpenOutline
                                            className={e.enabled ? s.editorIsActive : s.editorDontActive}
                                        />
                                    ) : (
                                        <IoLockClosedOutline
                                            className={e.enabled ? s.editorIsActive : s.editorDontActive}
                                        />
                                    )}
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>

                <Popup
                    isOpened={viewPopup}
                    children={
                        <div className={s.popup} ref={refPopup}>
                            <div>Введите почту для приглашения</div>
                            <form onSubmit={onSubmit} className={s.form}>
                                <div className={s.errInp}>
                                    {errors.email && <span className={s.errorMsg}>Введите почту для приглашения</span>}
                                    <input
                                        type="email"
                                        placeholder={'Почта'}
                                        {...register('email', {
                                            required: true,
                                        })}
                                        id="email"
                                        className={s.input}
                                    />
                                    {editors.errorMessage && <span className={s.errorMsg}>{editors.errorMessage}</span>}
                                </div>
                                <div className={s.btns}>
                                    <button className={s.btn} type="submit">
                                        Пригласить
                                    </button>
                                    <div className={cn(s.btn, s.btnCanel)} onClick={() => setViewPopup(false)}>
                                        Отменить
                                    </div>
                                </div>
                            </form>
                        </div>
                    }
                />
            </div>
        </>
    );
};

export default Editors;
