import React, { FC, useEffect, useState } from 'react';
import { FiArrowDownRight, FiArrowUpRight } from 'react-icons/fi';
import { NewsType } from '../../constants/enums';
import { getNewsThunk } from '../../store/slices/newsSlice';
import { useAppDispatch, useAppSelector } from '../../shared/hooks';
import { setNewsType, setSortDirection } from '../../store/slices/searchSlice';
import CardNews from '../../components/screens/news/card/cardNews';
import s from './publications.module.css';

const Published: FC = () => {
    const dispatch = useAppDispatch();
    const news = useAppSelector((state) => state.news);
    const params = useAppSelector((state) => state.search);
    const [dateSort, setDateSort] = useState(params.sortDirection == 1);

    useEffect(() => {
        dispatch(setSortDirection(dateSort ? 1 : -1));
        dispatch(setNewsType(NewsType.Published));
        dispatch(getNewsThunk());
    }, [dateSort]);

    return (
        <>
            <table className={s.table}>
                <thead>
                    <tr className={s.titles}>
                        <th>Новость</th>
                        <th onClick={() => setDateSort(!dateSort)} className={s.setDateSort}>
                            Дата публикации
                            <span>{dateSort ? <FiArrowUpRight /> : <FiArrowDownRight />}</span>
                        </th>
                        <th>Тег</th>
                        <th>Расположение</th>
                        <th>Просмотры</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {news.foundEntities.map((n) => (
                        <CardNews n={n} newsType={NewsType.Published} key={n.id} />
                    ))}
                </tbody>
            </table>
        </>
    );
};

export default Published;
