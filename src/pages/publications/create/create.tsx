import React, { FC } from 'react';
import { Link } from 'react-router-dom';
import CreateNews from '../../../components/screens/createNews/createNews';
import s from './create.module.css';

const Create: FC = () => {
    return (
        <>
            <div className={s.content}>
                <div className={s.header}>
                    <Link to={'/'} className={s.name}>
                        <div>Контур</div>
                        <div className={s.nameExtern}>Экстерн</div>
                    </Link>
                </div>

                <CreateNews />
            </div>
        </>
    );
};

export default Create;
