import { FC, useEffect, useState } from 'react';
import cn from 'classnames';
import { useForm } from 'react-hook-form';
import { useLocation, useNavigate } from 'react-router-dom';
import { AiOutlineEye, AiOutlineEyeInvisible } from 'react-icons/ai';
import { Auth } from '../../constants/types';
import { authLoginThunk } from '../../store/slices/authSlice';
import { useAppDispatch, useAppSelector } from '../../shared/hooks';
import s from './auth.module.css';

const Login: FC = () => {
    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm<Auth>();
    const [passwordView, setPasswordView] = useState(false);

    const dispatch = useAppDispatch();

    const onSubmit = handleSubmit((data) => {
        dispatch(authLoginThunk(data));
    });

    const auth = useAppSelector((state) => state.auth);

    const navigate = useNavigate();
    const location = useLocation();
    const fromPage = location.state?.from?.pathname || '/';
    useEffect(() => {
        if (localStorage.getItem('accessToken')) navigate('/');
    }, []);
    useEffect(() => {
        if (auth.isAuth) navigate(fromPage, { replace: true });
    }, [auth.accessToken, fromPage, navigate]);

    return (
        <>
            <div className={s.body}>
                <form className={s.form} onSubmit={onSubmit}>
                    <div className={s.name}>
                        <div className={s.kontur}>Контур</div>
                        <div className={s.title}>Вход в Экстерн</div>
                    </div>

                    <div className={s.formItem}>
                        <label htmlFor="email" className={s.label}>
                            Почта
                        </label>
                        {errors.email && <span className={s.errorMsg}>Введите почту</span>}
                        <input {...register('email', { required: true })} type="email" id="email" className={s.input} />
                    </div>
                    <div className={s.formItem}>
                        <label htmlFor="password" className={s.label}>
                            Пароль
                        </label>
                        <div className={s.inputEye}>
                            <input
                                {...register('password', { required: true })}
                                type={passwordView ? 'text' : 'password'}
                                id="password"
                                className={cn(s.input, s.brn)}
                            />
                            {errors.password && <span className={s.errorMsg}>Введите пароль</span>}
                            <div className={s.eye} onClick={() => setPasswordView(!passwordView)}>
                                {passwordView ? <AiOutlineEye /> : <AiOutlineEyeInvisible />}
                            </div>
                        </div>
                    </div>

                    {auth.error && (
                        <span className={s.errorMsgAuth}>
                            Мы вас не узнали. Проверьте, правильно ли вы указали почту и пароль
                        </span>
                    )}

                    <button className={s.authButton} type="submit">
                        Войти
                    </button>
                </form>
            </div>
        </>
    );
};

export default Login;
