import { FC, useEffect, useState } from 'react';
import cn from 'classnames';
import { useForm } from 'react-hook-form';
import { useLocation, useNavigate } from 'react-router-dom';
import { AiOutlineEye, AiOutlineEyeInvisible } from 'react-icons/ai';
import { Auth, AuthState } from '../../constants/types';
import { useAppDispatch, useAppSelector } from '../../shared/hooks';
import { authRegistrationThunk, incrementByAmount } from '../../store/slices/authSlice';
import s from './auth.module.css';

const Register: FC = () => {
    const {
        register,
        handleSubmit,
        watch,
        formState: { errors },
    } = useForm<Auth>();
    const [passwordView, setPasswordView] = useState(false);

    const dispatch = useAppDispatch();

    const onSubmit = handleSubmit((data) => {
        const registrationSummit: AuthState = {
            email: data.email,
            password: data.password,
            token: token,
        };
        dispatch(authRegistrationThunk(registrationSummit));
    });

    const auth = useAppSelector((state) => state.auth);

    const navigate = useNavigate();
    const location = useLocation();
    const params = new URLSearchParams(location.search);

    const email = params.get('email') || '';
    const token = params.get('token') || '';
    const urlParams: AuthState = { email, token };
    dispatch(incrementByAmount(urlParams));

    useEffect(() => {
        if (localStorage.getItem('accessToken')) navigate('/');
    }, []);

    useEffect(() => {
        if (auth.isAuth) navigate('/', { replace: true });
    }, [auth.isAuth]);

    return (
        <>
            <div className={s.body}>
                <form className={s.form} onSubmit={onSubmit}>
                    <div className={s.name}>
                        <div className={s.kontur}>Контур</div>
                        <div className={s.title}>Вход в Экстерн</div>
                    </div>

                    <div className={s.formItem}>
                        <label htmlFor="email" className={s.label}>
                            Почта
                        </label>
                        {errors.email && <span className={s.errorMsg}>Введите почту</span>}
                        <input
                            {...register('email', { required: true })}
                            defaultValue={auth.email}
                            type="email"
                            id="email"
                            className={s.input}
                        />
                    </div>
                    <div className={s.formItem}>
                        <label htmlFor="password" className={s.label}>
                            Пароль
                        </label>
                        <div className={s.inputEye}>
                            <input
                                {...register('password', { required: true })}
                                type={passwordView ? 'text' : 'password'}
                                id="password"
                                className={cn(s.input, s.brn)}
                            />
                            {errors.password && <span className={s.errorMsg}>Введите пароль</span>}
                            <div className={s.eye} onClick={() => setPasswordView(!passwordView)}>
                                {passwordView ? <AiOutlineEye /> : <AiOutlineEyeInvisible />}
                            </div>
                        </div>
                    </div>
                    <div className={s.formItem}>
                        <label htmlFor="passwordRepeat" className={s.label}>
                            Повторите пароль
                        </label>
                        {errors.passwordRepeat && <span className={s.errorMsg}>Пароли не совпадают</span>}
                        <div className={s.inputEye}>
                            <input
                                {...register('passwordRepeat', {
                                    required: true,
                                    validate: (value) => value === watch('password'),
                                })}
                                type={passwordView ? 'text' : 'password'}
                                id="passwordRepeat"
                                className={cn(s.input, s.brn)}
                            />
                            <div className={s.eye} onClick={() => setPasswordView(!passwordView)}>
                                {passwordView ? <AiOutlineEye /> : <AiOutlineEyeInvisible />}
                            </div>
                        </div>
                    </div>

                    <button className={s.authButton} type="submit">
                        Зарегистрироваться
                    </button>
                </form>
            </div>
        </>
    );
};

export default Register;
