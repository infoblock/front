import React, { FC } from 'react';
import s from './statistic.module.css';

const Statistics: FC = () => {
    return (
        <>
            <div className={s.wrapper}>
                <div className={s.img}></div>
                <div className={s.text}>
                    <div>
                        Ой!
                        <br /> Здесь должна быть потрясающая страница, <br />
                        но разработчики ещё не поняли, как сделать её ещё лучше
                    </div>
                    <div>Пожалуйста, загляните позже :)</div>
                </div>
            </div>
        </>
    );
};

export default Statistics;
